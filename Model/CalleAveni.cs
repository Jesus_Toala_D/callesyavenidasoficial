﻿using System;
using System.Collections.Generic;
using System.Text;
//Se usa este fichero para que nos permita trabajar de una manera correcta
using System.ComponentModel;

namespace CallesYAvenidasOF.Model
{
    public class CalleAveni { }
    //Se crea la clase CallesAvenid junto a la clase base
    //INotifyPropertyChanged donde esta notifica cuando se ha cambiado 
    //un valor en una propiedad.
    public class CallesAvenid : INotifyPropertyChanged
    {
        public string calle;
        public string avenida;

        public string Calle
        {
            get
            {
                return calle;
            }
            set
            {
                if (calle != value)
                {
                    calle = value;
                    //A raiz de la clase INotifyPropertyChanged, esta genera un evento 
                    // PropertyChanged cuando cambia el valor de una de las propiedades por ejemplo cuando se cambia el valor de calle.
                    RaisePropertyChanged("Calle");
                    RaisePropertyChanged("CalleYAvenida");
                }
            }

        }
        public string Avenida
        {
            get
            {
                return avenida;
            }

            set
            {
                if (avenida != value)
                {
                    avenida = value;
                    //A raiz de la clase INotifyPropertyChanged, esta genera un evento 
                    // PropertyChanged cuando cambia el valor de una de las propiedades por ejemplo cuando se cambia el valor de Avenida.
                    RaisePropertyChanged("Avenida");
                    RaisePropertyChanged("CalleYAvenida");
                }
            }

        }

        public string CalleYAvenida
        {

            get
            {
                return calle + " " + avenida;
            }
        }

        //Se define el atributo PropertyChangedEventHandler como manejador del evento 
        //para notificar a las clases cuando cambia una propiedad en alguno de los atributos editables en este caso Calle y Avenida
        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChanged(string property)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(property));
            }
        }
    }
}
