﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Collections.ObjectModel;
using CallesYAvenidasOF.Model;
namespace CallesYAvenidasOF.ViewModel
{
    class CalleAveniVM
    {
        //Se crea el atributo ObservableCollection donde representa una coleccion
        //de datos dinamicos de la clase CallesAvenid.
        public ObservableCollection<CallesAvenid> callesavenidas
        {
            get;
            set;
        }

        public void CargaCalles()
        {
            //Creamos una lista donde cargara la coleccion de datos con los datos
            //de las calles y avenidas
            ObservableCollection<CallesAvenid> callesaveni = new ObservableCollection<CallesAvenid>();
            callesaveni.Add(new CallesAvenid { Calle = "Calle 8", Avenida = "Avenida 20" });
            callesaveni.Add(new CallesAvenid { Calle = "Calle 13", Avenida = "Avenida 19" });
            callesaveni.Add(new CallesAvenid { Calle = "Calle 8", Avenida = "Avenida 28" });
            callesaveni.Add(new CallesAvenid { Calle = "Calle 11", Avenida = "Avenida 24" });
            callesavenidas = callesaveni;
        }
    }
}
